/**
 * Created by prince on 10/7/15.
 */
module.exports = {
    CategoryService : require('./CategoryService'),
    CollectionsService : require('./CollectionsService'),
    AdminService : require('./AdminService'),
    AppVersionService : require('./AppVersionService'),
    UserService : require('./UserService'),
    CustomerService : require('./CustomerService'),
    DetailsService : require('./DetailsService'),
    WorkerService : require('./WorkerService')
};