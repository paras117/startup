'use strict';

var Models = require('../Models');

//Get Users from DB
var getWorkers = function (criteria, projection, options, callback) {
    Models.Workers.find(criteria, projection, options, callback);
};


var updateWorkers = function (criteria, dataToSet, options, callback) {
    Models.Workers.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Get All Generated Codes from DB


//Insert User in DB
var createWorkers = function (objToSave, callback) {
    new Models.Workers(objToSave).save(callback)
};

//Delete User in DB
var deleteWorkers = function (criteria, callback) {
    Models.Workers.findOneAndRemove(criteria, callback);
};


module.exports = {
    getWorkers:getWorkers,
    updateWorkers:updateWorkers,
    createWorkers:createWorkers,
    deleteWorkers:deleteWorkers
};
