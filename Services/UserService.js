'use strict';

var Models = require('../Models');

//Get Users from DB
var getUsers = function (criteria, projection, options, callback) {
    Models.Users.find(criteria, projection, options, callback);
};


var updateUsers = function (criteria, dataToSet, options, callback) {
    Models.Users.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Get All Generated Codes from DB


//Insert User in DB
var createUsers = function (objToSave,projection,options,callback) {
    new Models.Users(objToSave).save(function(err,result)
    {
        if(err)
        {
            console.log("error",err)
            callback(err)

        }
        else{
            callback(null);
        }

    })
}

//Delete User in DB
var deleteUsers = function (criteria, callback) {
    Models.Users.findOneAndRemove(criteria, callback);
};


module.exports = {
    deleteUsers:deleteUsers,
    createUsers:createUsers,
    updateUsers:updateUsers,
    getUsers:getUsers
};

