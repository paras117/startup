'use strict';

var Models = require('../Models');

//Get Users from DB
var getCustomer = function (criteria, projection, options, callback) {
    Models.Customer.find(criteria, projection, options, callback);
};


var updateCustomer = function (criteria, dataToSet, options, callback) {
    Models.Customer.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Get All Generated Codes from DB


//Insert User in DB
var createCustomer = function (objToSave, callback) {
    new Models.Customer(objToSave).save(callback)
};

//Delete User in DB
var deleteCustomer = function (criteria, callback) {
    Models.Customer.findOneAndRemove(criteria, callback);
};

var createBooking = function (objToSave, callback) {
    new Models.Booking(objToSave).save(callback)
};

var getBooking = function (criteria, projection, options, callback) {
    Models.Booking.find(criteria, projection, options, callback);
};

var updateBooking = function (criteria, dataToSet, options, callback) {
    Models.Booking.findOneAndUpdate(criteria, dataToSet, options, callback);
};


module.exports = {
    getCustomer:getCustomer,
    updateCustomer:updateCustomer,
    createCustomer:createCustomer,
    deleteCustomer:deleteCustomer,
    getBooking:getBooking,
    createBooking:createBooking
};

