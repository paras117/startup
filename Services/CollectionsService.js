'use strict';

var Models = require('../Models');

//Get Users from DB
var getCollections = function (criteria, projection, options, callback) {
    Models.Collections.find(criteria, projection, options, callback);
};

//Insert User in DB
var createCollections = function (objToSave, callback) {
    new Models.Collections(objToSave).save(callback)
};

//Get All Generated Codes from DB


//Update User in DB
var updateCollections = function (criteria, dataToSet, options, callback) {
    Models.Collections.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteCollections= function (criteria, callback) {
    Models.Collections.findOneAndRemove(criteria, callback);
};

module.exports = {
    deleteCollections:deleteCollections,
    updateCollections:updateCollections,
    createCollections:createCollections,
    getCollections:getCollections
};

