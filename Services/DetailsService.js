/**
 * Created by paras on 20/5/16.
 */

'use strict';

var Models = require('../Models');

//Get Users from DB
var getDetails = function (criteria, projection, options, callback) {
    Models.Details.find(criteria, projection, options, callback);
};


var updateDetails = function (criteria, dataToSet, options, callback) {
    Models.Details.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Get All Generated Codes from DB


//Insert User in DB
var createDetails = function (objToSave,callback) {
    new Models.Details(objToSave).save(function(err,result)
    {
        if(err){
            console.log("error",err);
            callback(err)
        }
        else{
            callback(null);
        }

    })
};

//Delete User in DB
var deleteDetails = function (criteria, callback) {
    Models.Details.findOneAndRemove(criteria, callback);
};


module.exports = {
    deleteDetails:deleteDetails,
    createDetails:createDetails,
    updateDetails:updateDetails,
    getDetails:getDetails
};

