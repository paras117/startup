'use strict';

var Models = require('../Models');

//Get Users from DB
var getCategory = function (criteria, projection, options, callback) {
    Models.Categories.find(criteria, projection, options, callback);
};


var updateCategories = function (criteria, dataToSet, options, callback) {
    Models.Categories.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Get All Generated Codes from DB


//Insert User in DB
var createCategories = function (objToSave,callback) {
    new Models.Categories(objToSave).save(function(err,result)
    {
        if(err){
            console.log("error",err);
            callback(err)
        }
        else{
            callback(null);
        }

    })
};

//Delete User in DB
var deleteCategories = function (criteria, callback) {
    Models.Categories.findOneAndRemove(criteria, callback);
};


module.exports = {
    deleteCategories:deleteCategories,
    createCategories:createCategories,
    updateCategories:updateCategories,
    getCategory:getCategory
};

