'use strict';
/**
 * Created by paras on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [

    {
        method: 'GET',
        path: '/api/collections/listing',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.CollectionsController.listCollections(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.LISTING, data)).code(201)
                }
            });
        },
        config: {
            description: 'List Collections ||  WARNING : Will not work from documentation, use postman instead',
            tags: ['api', 'collections'],
            validate: {
                query: {
                    categoryId: Joi.required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }

];