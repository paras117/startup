'use strict';
/**
 * Created by prince on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
        {
        method: 'POST',
        path: '/api/DemoWorker/login',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DemoWorkerController.login(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'worker login ',
            tags: ['api', 'demo worker'],
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password:Joi.string().required(),
                    lat : Joi.number().required(),
                    long: Joi.number().required(),
                    deviceToken:Joi.number().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/DemoWorker/signUp',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DemoWorkerController.signUp(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'worker login ',
            tags: ['api', 'demo worker'],
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password:Joi.string().required(),
                    name:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/DemoWorker/ApproveOrRejectJob',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DemoWorkerController.ApproveOrRejectJob(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'worker login ',
            tags: ['api', 'demo worker'],
            validate: {
                payload: {
                    id: Joi.string().length(24).required(),
                    status:Joi.boolean().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];