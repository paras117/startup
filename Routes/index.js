/**
 * Created by prince on 10/7/15.
 */
'use strict';

var AdminRoute = require('./AdminRoute');
var CategoryRoute = require('./CategoryRoute');
var CollectionRoute = require('./CollectionRoute');
var DemoCustomer = require('./DemoCustomer');
var Details = require('./DetailsRoute');
var DemoWorker = require('./DemoWorker');

var all = [].concat(AdminRoute,CategoryRoute,CollectionRoute,DemoCustomer,Details,DemoWorker);

module.exports = all;

