'use strict';
/**
 * Created by paras on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [

    {
        method: 'POST',
        path: '/api/DemoCustomer/SignUp',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DemoCustomerController.signUp(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'customer signup ',
            tags: ['api', 'demo customer'],
            validate: {
                payload: {
                    name: Joi.string().required(),
                    lat:  Joi.number().required(),
                    lon:  Joi.number().required(),
                    phoneNo: Joi.number().required(),
                    email:Joi.string().required(),
                    devicetoken:Joi.string().required(),
                    password:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
        {
        method: 'POST',
        path: '/api/DemoCustomer/login',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.DemoCustomerController.login(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'customer login ',
            tags: ['api', 'demo customer'],
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password:Joi.string().required(),
                    deviceToken:Joi.string().required(),
                    lat:Joi.number().required(),
                    long:Joi.number().required(),
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/DemoCustomer/Booking',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.DemoCustomerController.booking(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'customer Booking ',
            tags: ['api', 'demo customer'],
            validate: {
                query: {
                    email: Joi.string().required(),
                    _id:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/DemoCustomer/getAllDriver',
        handler: function (request, reply) {
            var payloadData = request.query;
            Controller.DemoCustomerController.getAllDriver(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'customer Booking ',
            tags: ['api', 'demo customer'],
            validate: {
                query: {
                    _id:Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];