
 

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    
    {
        method: 'GET',
        path: '/api/categories/listing',
        handler: function (request, reply) {
            var payloadData = request.query;
            console.log("payloaddata",payloadData)
            Controller.CategoryController.listCategories(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.LISTING, data)).code(201)
                }
            });
        },
        config: {
            description: 'List categories ||  WARNING : Will not work from documentation, use postman instead',
            tags: ['api', 'categories','listing'],
            validate: {
                query: {
                    deviceToken: Joi.string().required().trim(),
                    latitude: Joi.number().optional(),
                    longitude: Joi.number().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];
