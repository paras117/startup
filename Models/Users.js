var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Users = new Schema({
    deviceToken:{type: String, trim: true, index: true, default: null, sparse: true},
    lat: {type: Number},
    long:{type: Number}
});

module.exports = mongoose.model('Users',Users);