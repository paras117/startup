var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Categories = new Schema({
 name: {type: String, trim: true, index: true, default: null, sparse: true},
    collections: {type: Schema.ObjectId, ref: 'Collections'}
});


module.exports = mongoose.model('Categories',Categories);