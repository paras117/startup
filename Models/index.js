/**
 * Created by prince on 10/7/15.
 */
module.exports = {
    Admins : require('./Admins'),
    AppVersions : require('./AppVersions'),
    Users : require('./Users'),
    Categories : require('./Categories'),
    Collections : require('./Collections'),
    Customer : require('./Customer'),
    Details : require('./Details'),
    Workers : require('./Workers'),
    booking : require('./Booking')
};