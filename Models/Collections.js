var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Collections = new Schema({
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    description: {type: String, trim: true, default: null, sparse: true},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    categories: {type: Schema.ObjectId, ref: 'Categories', required: true}
});


module.exports = mongoose.model('Collections',Collections);