var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Booking = new Schema({
    status:{type:Boolean},
    customer: {type: Schema.ObjectId, ref: 'Customer'},
    driver:{type: Schema.ObjectId, ref: 'Workers'}
});


module.exports = mongoose.model('Booking',Booking);