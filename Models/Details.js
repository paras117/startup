/**
 * Created by paras on 20/5/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Details = new Schema({
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    description: {type: String, trim: true, default: null, sparse: true},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    collections: {type: Schema.ObjectId, ref: 'Collections', required: true}
});


module.exports = mongoose.model('Details',Details);
