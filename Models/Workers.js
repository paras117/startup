var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Workers = new Schema({
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    email:{type: String, trim: true, index: true, default: null, sparse: true,unique:true},
    lat:{type:Number},
    long:{type:Number},
    phoneNo:{type:Number,requird:true,unique:true},
    timeStamp:{type:Date},
    deviceToken:{type: String,unique:true},
    password:{type: String},
    isLogin:{type:Boolean}
});


module.exports = mongoose.model('Workers',Workers);