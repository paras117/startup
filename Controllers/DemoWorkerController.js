'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');



var signUp = function (payloadData, callback) {
    var collections;
    var result;
    async.auto({
           insertValue:function(cb){
                Service.WorkerService.createWorkers(payloadData, function (err, data) {
                if (err) {
                  cb(err)
                 } else {
                  result  = data;
                  cb(null,result);
          }
            })
           }
    },function (err, data) {
       if(err){
          callback(err);
       }else{
          callback(null,data);
       } 
    });
};


var login = function(payloadData,cb){
 var result;
 var id;
  async.auto({
      login:function(callback){
        var query = {
              email:payloadData.email,
              password:payloadData.password
        };
        var options = {lean:true};
        var projections = {};
     Service.WorkerService.getWorkers(query,projections,options,function (err, data) {
                if (err) {
                  callback(err)
                 } else {
                  if(data.length){
                    id = data[0]._id;
                           result  = data;
                            callback(null,result);
                         }else{
                            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                         }
              }
            })

      },
      saveLatLong:['login',function(callback){
        var addToSet = {
          lat:payloadData.lat,
          long :payloadData.long, 
            deviceToken:payloadData.deviceToken  
        }

        var query = {_id:id};
        var options = {new:true};
        Service.WorkerService.updateWorkers(query,addToSet,options,function(err,result){
          if(err){
              callback(err);
          }else{
              callback(null);
          }
        })
      }]

  },function(err,data){
    if(err){
      cb(err);
    }else{
      cb(null,result);
    }
  })
}


var ApproveOrRejectJob = function(payloadData,cb){
    var flag = 0;
    var data;
    var driverId;
    var customerId;
    var deviceToken;
    async.auto({
        checkBooking:function(callback){
            var query = {_id:payloadData.id,
                status:false
            };
            var options = {lean:true};
            var projeections = {_id:1,
                customer:1
            };
            Service.CustomerService.getBooking(query,projeections,options,function(err,result){
                if(err){
                    callback(err);
                }else{
                 if(result.length){
                     flag = 1;
                     driverId = result[0]._id;
                     data = result;
                     callback(null);
                 }else{
                     callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.BOOKING)
                 }
                }
            })
        },
        approvebooking:['checkBooking',function(callback){
            var query = {_id:payloadData.id};
            var options = {lean:true};
            var addToSet = {
                status:true,
                driver:driverId
            };
            Service.CustomerService.updateBooking(query,addToSet,options,function(err,result){
                if(err){
                    callback(err);
                }else{
                    customerId = result[0].customer;
                    callback(null);
                }
            })
        }],
        getCustomerDetails:['approvebooking',function(callback){
            var query = {_id:customerId};
            var options = {lean:true};
            var projections = {
                deviceToken:1
            };
            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    callback(err);
                }else{
                    deviceToken = result[0].devicetoken;
                    callback(null);
                }
            })
        }],
        sendPush:['getCustomerDetails',function(callback){
            // send push record
            callback(null);
        }]
    },function(err,result){
        if(err){
            cb(err);
        }else{
            cb(null,data);
        }
    })
}


module.exports = {
    signUp: signUp,
    login:login,
    ApproveOrRejectJob:ApproveOrRejectJob
};