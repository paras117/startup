'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var print = require('geo_decoder');







var signUp = function (payloadData, callback) {
    var collections;
    var result;
    async.auto({
           insertValue:function(cb){
                Service.CustomerService.createCustomer(payloadData, function (err, data) {
                if (err) {
                  cb(err)
                 } else {
                  result  = data;
                  cb(null,result);
          }
            })
           }
    },function (err, data) {
       if(err){
          callback(err);
       }else{
          callback(null,data);
       } 
    });
};


var login = function(payloadData,cb){
    var result;
    var id;
    async.auto({
        login:function(callback){
            var query = {
                email:payloadData.email,
                password:payloadData.password
            };
            var options = {lean:true};
            var projections = {};
            Service.CustomerService.getCustomer(query,projections,options,function (err, data) {
                if (err) {
                    callback(err)
                } else {
                    if(data.length){
                        id = data[0]._id;
                        result  = data;
                        callback(null,result);
                    }else{
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_USER_PASS)
                    }
                }
            })
        },
        updateValue:['login',function(callback){
            var query = {_id:id };
            var addToSet = {
                devicetoken:payloadData.devicetoken,
                long:payloadData.long,
                lat:payloadData.lat,
                isLogin:true
            }
            var options = {new:true};
            Service.CustomerService.updateCustomer(query,addToSet,options,function(err,result){
                if(err){
                    callback(err);
                }else{
                    callback(err,result);
                }
            })
        }]
    },function(err,result){
        if(err){
            cb(err);
        }else{
            cb(null,result);
        }
    })
}


var booking = function(payloaddata,cb){
    var user = {};
    var workerData;
    var freeDriver= [];
    async.auto({
        getMyLatLong:function(callback){
            var query = {
                email:payloaddata.email,
                _id:payloaddata.id
            }
            var options = {lean:true};
            var projections = {
                _id:1,
                lat:1,
                long:1
            };
            Service.CustomerService.getCustomer(query,projections,options,function(err,result){
                if(err){
                    callback(err);
                }else{
                    if(result.lengt){
                        user.id = result[0]._id;
                        user.lat = result[0].lat;
                        user.long = result[0].long;
                        callback(null);

                    }else{
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_ID)
                    }
                }
            })
        },
        createBooking:['getMyLatLong',function(callback){
            var object = {
                status:false,
                customer:user.id
            }
            Service.CustomerService.createBooking(object,function(err,result){
                if(err){
                    callback(err);
                }else{
                    callback(null);
                }
            })
        }],
        getDriverData:['getMyLatLong',function(callback){
            var query = {isLogin:true};
            var options = {lean:true};
            var projections = {
                lat:1,
                long:1,
                deviceToken:1,
                _id:1
            };
            Service.WorkerService.getWorkers(query,projections,options,function(err,result){
                if(err){
                    cb(err);
                }else{
                    workerData = result;
                    callback(null);
                }
            })
        }],
        checkNearByDriver:['getDriverData',function(callback){
            var len = workerData.length;
            for(var i = 0;i < len;i++){
                (function(i){
                    var p1 = {
                        x: workerData[i].lat,
                        y: workerData[i].long
                    };
                    var p2 = {
                        x: user.lat,
                        y: user.long
                    };
                    var dist1 = print.distance(p1, p2);
                    console.log("Distance between: ", p1, " and ", p2, " is:", dist1);
                    if(dist1 <= 5){
                        freeDriver.push({id:workerData[i]._id,deviceToken:workerData[i].deviceToken});                
                    }
                }(i));
                
            }
            if(i == len){
                callback(null);
            }
        }],
        sendPush:['checkNearByDriver',function(callback){
            //write push function heere
            callback(null);
        }]
    },function(err,result){
        if(err){
            cb(err);
        }else{
            cb(null,result);
        }
    })
}

var getAllDriver  =function(payloadData,cb){
    var data;
    async.auto({
        getDriverLatLong:function(callback){
            var query = {isLogin:true};
            var projection = {
                _id:1,
                long:1,
                lat:1
            };
            var options = {lean:true};
            Service.WorkerService.getWorkers(query,projection,options,function(err,result){
                if(err){
                    callback(err);
                }else{
                     data = result;
                    callback(null);
                }
            })
        }
    },function(err,result){
        if(err){
            cb(err);
        }else{
            cb(null,data);
        }
    })
}

module.exports = {
    signUp: signUp,
    login:login,
    getAllDriver:getAllDriver,
    booking:booking
};