'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');



var listCollections = function (payloadData, callback) {
    var collections;
    async.auto({
           getCollections: function (cb) {
               var criteria = {
                   categories : payloadData.categoryId
               };
               var projection = {};
               var option = {
                   lean: true
               };
               Service.CollectionsService.getCollections(criteria, projection, option, function (err, result) {
                   if (err) {
                       cb(err)
                   } else {
                       collections = result;
                       cb();
                   }
               });
            },
    },

     function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                collections: collections
            });
        }
    });
};


var insertCollections = function (payloadData, callback) {

    async.auto({
            authenticateAccessToken : function(cb){
                var criteria = {
                    accessToken:payloadData.accessToken
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.AdminService.getAdmin(criteria,projection,option,function (err, result) {
                    if (err)
                    {
                        console.log("erorr",err);
                        cb(err)
                    } else {
                        if(result.length)
                        {
                            cb(null);
                        }
                        else{
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }

                    }
                });

            },
            insertCollections:['authenticateAccessToken',function (cb) {
                var criteria = {
                    categories : payloadData.categoryId,
                    name: payloadData.name,
                    description:payloadData.description
                };
                Service.CollectionsService.createCollections(criteria,function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        cb();
                    }
                });
            }]
        },

        function (err, data) {
            if (err) {
                callback(err);
            } else {
                callback(null,{}
                );
            }
        });
};


module.exports = {
    listCollections: listCollections,
    insertCollections:insertCollections
};