/**
 * Created by paras on 20/5/16.
 */

'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');


var listDetails = function(payloadData,callback){
    var details;
    async.auto({
            getDetails: function (cb) {
                var criteria = {
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.DetailsService.getDetails(criteria, projection, option, function (err, result) {
                    if (err) {
                        console.log("erorr",err);
                        cb(err)
                    } else {
                        details = result;
                        cb(null);
                    }
                });
            }
        }
        , function (err, data) {
            if (err) {
                callback(err);
            } else {
                callback(null,{"data":details});
            }
        });
};

var insertDetails = function(payloadData,callback){
    async.auto({

            authenticateAccessToken : function(cb){
                var criteria = {
                    accessToken:payloadData.accessToken
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.AdminService.getAdmin(criteria,projection,option,function (err, result) {
                    if (err)
                    {
                        console.log("erorr",err);
                        cb(err)
                    } else {
                        if(result.length)
                        {
                            cb(null);
                        }
                        else{
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }

                    }
                });

            },
            insertDetails: ['authenticateAccessToken',function (cb) {
                var criteria = {
                    name:payloadData.name,
                    description:payloadData.description,
                    collections:payloadData.collectionId
                };
                Service.DetailsService.createDetails(criteria,function (err, result) {
                    if (err)
                    {
                        console.log("erorr",err)
                        cb(err)
                    } else {
                        cb(null);
                    }
                });
            }]
        }
        , function (err, data) {
            if (err) {
                callback(err);
            } else{
                callback(null,{});
            }
        });
};

module.exports = {
    listDetails: listDetails,
    insertDetails:insertDetails
};