'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');


var listCategories = function(payloadData,callback){
   var categories;
    async.auto({
           insertUserDetails: function (cb) {
                var criteria = {
                    deviceToken : payloadData.deviceToken,
                    long: payloadData.longitude,
                    lat:payloadData.latitude
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.UserService.createUsers(criteria, projection, option, function (err, result) {
                    if (err)
                    {
                        console.log("erorr",err)
                        cb(err)
                    } else {
                        cb(null);
                    }
                });
            },
            getCategories: function (cb) {
                var criteria = {
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.CategoryService.getCategory(criteria, projection, option, function (err, result) {
                    if (err) {
                        console.log("erorr",err)
                        cb(err)
                    } else {
                        categories = result;
                        cb(null);
                    }
                });
            }
    }
    , function (err, data) {
        if (err) {
            callback(err);
        } else {
            console.log("categories",categories)
            callback(null,{"data":categories});
        }
    });
};


var insertCategories = function(payloadData,callback){
    async.auto({

            authenticateAccessToken : function(cb){
                var criteria = {
                    accessToken:payloadData.accessToken
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.AdminService.getAdmin(criteria,projection,option,function (err, result) {
                    if (err)
                    {
                        console.log("erorr",err);
                        cb(err)
                    } else {
                        if(result.length)
                        {
                            cb(null);
                        }
                        else{
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                        }

                    }
                });

            },
            insertCategory: ['authenticateAccessToken',function (cb) {
                var criteria = {
                    name:payloadData.name
                };
                Service.CategoryService.createCategories(criteria,function (err, result) {
                    if (err)
                    {
                        console.log("erorr",err)
                        cb(err)
                    } else {
                        cb(null);
                    }
                });
            }]
        }
        , function (err, data) {
            if (err) {
                callback(err);
            } else{
                callback(null,{});
            }
        });
};

module.exports = {
    listCategories: listCategories,
    insertCategories:insertCategories
};