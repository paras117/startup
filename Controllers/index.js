/**
 * Created by paras on 10/7/15.
 */
module.exports  = {
    CategoryController : require('./CategoryController'),
    AdminController : require('./AdminController'),
    AppVersionController : require('./AppVersionController'),
    CollectionsController : require('./CollectionsController'),
    DemoCustomerController : require('./DemoCustomerController'),
    DetailsController:require('./DetailsController')
};